using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject wordprefab;

    [SerializeField]
    private Transform wordPosition;


    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Talk();
        }
    }



    private void Talk()
    {
        //Instantiate(wordprefab, wordPosition.position, Quaternion.identity);

        GameObject word = ObjectPool.instance.GetPooledObject();
        if (word != null)
        {
            word.transform.position = wordPosition.position;
            word.SetActive(true);
        }
           
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    
}
